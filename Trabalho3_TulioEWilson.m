% Computacao Grafica
% UERJ - 2017.2 | 2018.1
% Trabalho 3
% Alunos: Tulio Loureiro
%         Wilson Valente

% Carrega pacote de imagem
pkg load image;
clear;

% Le a imagem deformada
placaTorta = im2double(imread('Placa_Estacionamento.png'));

% Coeficientes (xi yi x'i y'i) analisados
%
% i   (xi, yi)      (x'i, y'i)
% 0   (100, 100)    (175, 95)     CIMA- ESQUERDA
% 1   (100, 1000)   (30, 640)     BAIXO ESQUERDA
% 2   (450, 100)    (440, 20)     CIMA-DIREITA
% 3   (450, 1000)   (390, 612)    BAIXO-DIREITA
%
matriz = [100 100 175 95; 100 1000 30 640; 450 100 440 20; 450 1000 390 612];

% Observacoes (x'i y'i)
observacoes = [matriz(1,3) matriz(1,4) matriz(2,3) matriz(2,4) matriz(3,3) matriz(3,4) matriz(4,3) matriz(4,4)]';

% Sistema de Equacoes
coeficientes =  [matriz(1,1) matriz(1,2) 1 0 0 0 -(matriz(1,1)*matriz(1,3)) -(matriz(1,2)*matriz(1,3));
0 0 0 matriz(1,1) matriz(1,2) 1 -(matriz(1,1)*matriz(1,4)) -(matriz(1,2)*matriz(1,4));
matriz(2,1) matriz(2,2) 1 0 0 0 -(matriz(2,1)*matriz(2,3)) -(matriz(2,2)*matriz(2,3));
0 0 0 matriz(2,1) matriz(2,2) 1 -(matriz(2,1)*matriz(2,4)) -(matriz(2,2)*matriz(2,4));
matriz(3,1) matriz(3,2) 1 0 0 0 -(matriz(3,1)*matriz(3,3)) -(matriz(3,2)*matriz(3,3));
0 0 0 matriz(3,1) matriz(3,2) 1 -(matriz(3,1)*matriz(3,4)) -(matriz(3,2)*matriz(3,4));
matriz(4,1) matriz(4,2) 1 0 0 0 -(matriz(4,1)*matriz(4,3)) -(matriz(4,2)*matriz(4,3));
0 0 0 matriz(4,1) matriz(4,2) 1 -(matriz(4,1)*matriz(4,4)) -(matriz(4,2)*matriz(4,4))];

% L = A*x (queremos x)
x = inv(coeficientes' * coeficientes) * coeficientes' * observacoes;

% MatrizT transformacao (Original para Torta)
matrizT = [x(1) x(2) x(3); x(4) x(5) x(6); x(7) x(8) 1];


% Inicializa o shape vazio placaReta
placaReta = zeros(1600,800,3);
%placaBilinear = placaReta;


% Varre placaReta definindo intensidade do pixel
% Varre as 3 camadas de cor da imagem
for camada = 1:3
  % Varre cada linha
  for x = 1:size(placaReta)(1)
    
    % Varre cada coluna
    for y = 1:size(placaReta)(2)
    
      % Encontra ponto XY na placaTorta que corresponde ao ponto XY na placaReta
      pixelPlacaTorta = matrizT*[x;y;1];
      
      
      % Reconstrucao por vizinho mais proximo
      pixelPlacaTorta = [round(pixelPlacaTorta(1)/pixelPlacaTorta(3));
      round(pixelPlacaTorta(2)/pixelPlacaTorta(3));
      round(pixelPlacaTorta(3)/pixelPlacaTorta(3))];
      
      
      if ( ( pixelPlacaTorta(1) > 0 && pixelPlacaTorta(2) > 0 ) 
          && ( (pixelPlacaTorta(1) < size(placaTorta)(1)) && (pixelPlacaTorta(2) < size(placaTorta)(2)) ) )
      
          placaBilinear(x, y, camada) = placaTorta(pixelPlacaTorta(1), pixelPlacaTorta(2), camada);
          
      endif
      
    endfor
  endfor
endfor

% Mostra placaTorta
figure;
imshow(placaTorta);

% Mostra placaReta
%figure;
%imshow(placaReta);

% Mostra recontrucao da placaBilinear
figure;
imshow(placaBilinear);

% Mostra recontrucao da placaTriangular
%figure;
%imshow(placaTriangular);